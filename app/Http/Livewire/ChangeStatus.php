<?php

namespace App\Http\Livewire;

use App\Models\Project;
use App\Models\Status;
use App\Models\Task;
use Livewire\Component;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class ChangeStatus extends Component {

    public $item;
    public $statuses;
    public $statusID;
    public $css;

    public function mount($item, $statuses, $css = null) {
        $this -> item = $item;
        $this -> statuses = $statuses;
        $this -> css = $css;
    }

    public function render() {
        return view('livewire.change-status');
    }

    public function changeStatus($statusID) {
        if (Auth::guest() || Auth::user() -> cannot('view', $this -> item)) {
            abort(Response::HTTP_FORBIDDEN);
        }

        $exists = Status::where('id', $statusID) -> exists();

        if (!$exists && !($this -> item instanceof Task || $this -> item instanceof Project)) {
            abort(Response::HTTP_BAD_REQUEST);
        }

        $this -> item -> status_id = $statusID;
        $this -> item -> save();

        $this -> item -> refresh();

        $this -> emit('notificationSuccess', 'Status was updated!');
        $this -> emit('statusUpdated');
    }

}
