<?php

namespace App\Http\Livewire;

use App\Models\Priority;
use App\Models\Project;
use App\Models\Status;
use App\Models\Task;
use App\Models\UserProject;
use Livewire\Component;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class CreateTask extends Component {

    public $project;
    public $title;
    public $appointee;
    public $priority;
    public $status;
    public $description;

    protected $rules = [
        'title' => ['required', 'min:3', 'max:64'],
        'appointee' => ['required', 'exists:users,id'],
        'priority' => ['exists:priorities,id'],
        'status' => ['exists:statuses,id'],
        'description' => ['required', 'min:10']
    ];

    public function render() {
        $usersProjects = UserProject::with(['user', 'user.role']) -> where('project_id', $this -> project -> id) -> get();
        $priorities = Priority::all();
        $statuses = Status::all();

        return view('livewire.create-task', [
            'usersProjects' => $usersProjects,
            'priorities' => $priorities,
            'statuses' => $statuses
        ]);
    }

    public function mount(Project $project) {
        $this -> project = $project;
    }

    public function createTask() {
        if (Auth::guest() || Auth::user() -> isDeveloper()) {
            abort(Response::HTTP_FORBIDDEN);
        }

        $this -> validate();

        $newTask = Task::create([
            'project_id' => $this -> project -> id,
            'created_by' => auth() -> user() -> id,
            'title' => $this -> title,
            'assigned_to' => $this -> appointee,
            'priority_id' => $this -> priority,
            'status_id' => $this -> status,
            'description' => $this -> description
        ]);

        $this -> emit('notificationSuccess', 'Task was created successfully!');
        $this -> emit('taskCreated');

    }

}
