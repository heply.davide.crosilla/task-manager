<?php

namespace App\Http\Livewire;

use App\Models\Role;
use App\Models\User;
use Livewire\Component;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class CreateUser extends Component {

    public $name;
    public $role;
    public $email;
    public $password;

    protected $rules = [
        'name' => ['required', 'min:3', 'max:100'],
        'role' => ['required'],
        'email' => ['required', 'min:3', 'max:255', 'email'],
        'password' => ['required', 'min:8', 'max:20']
    ];

    public function render() {
        return view('livewire.create-user', [
            'roles' => Role::all()
        ]);
    }

    public function createUser() {
        if (Auth::guest() || Auth::user() -> isDeveloper()) {
            abort(Response::HTTP_FORBIDDEN);
        }

        $this -> validate();

        $newUser = User::create([
            'role_id' => $this -> role,
            'name' => $this -> name,
            'email' => $this -> email,
            'password' => Hash::make($this -> password)
        ]);

        $this -> emit('notificationSuccess', 'User was successfully created!');
        $this -> emit('userCreated');
    }

}
