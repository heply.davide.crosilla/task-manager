<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function projects() {
        return $this -> hasMany(UserProject::class);
    }

    public function tasks() {
        return $this -> hasMany(Task::class);
    }

    public function role() {
        return $this -> belongsTo(Role::class);
    }

    public function getAvatar() {
        return 'https://picsum.photos/id/' . $this -> id . '/400';
    }

    public function isProjectManager() {
        return $this -> role_id === 1;
    }

    public function isDeveloper() {
        return $this -> role_id === 2;
    }
}
