<?php

namespace Database\Factories;

use App\Models\Task;
use Illuminate\Database\Eloquent\Factories\Factory;

class TaskFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Task::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'created_by' => 1,
            'assigned_to' => 1,
            'priority_id' => 2,
            'status_id' => 2,
            'title' => ucwords($this -> faker -> words(4, true)),
            'description' => $this -> faker -> paragraph(),
        ];
    }
}
