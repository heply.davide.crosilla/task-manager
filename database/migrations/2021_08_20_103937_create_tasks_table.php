<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table -> id();
            $table -> unsignedBigInteger('created_by');
            $table -> foreign('created_by') -> references('id') -> on('users');
            $table -> unsignedBigInteger('assigned_to');
            $table -> foreign('assigned_to') -> references('id') -> on('users');
            $table -> foreignId('project_id') -> required();
            $table -> foreignId('priority_id') -> default(2);
            $table -> foreignId('status_id') -> default(2);
            $table -> string('title') -> required();
            $table -> text('description') -> required();
            $table -> timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
