<div class="flex flex-col my-4">
    @if ($projects)
        <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                <div class="overflow-hidden border border-gray-200 sm:rounded-xl">
                    <table class="min-w-full divide-y divide-gray-100 text-sm">
                        <thead class="bg-gray-50">
                            <tr>
                                <th scope="col">
                                    Title
                                </th>
                                <th scope="col">
                                    Client
                                </th>
                                <th scope="col">
                                    Status
                                </th>
                                <th scope="col">
                                    People
                                </th>
                                <th scope="col">
                                    Tasks
                                </th>
                            </tr>
                        </thead>
                        <tbody class="bg-white divide-y divide-gray-100">
                            @foreach ($projects as $project)
                                <tr class="cursor-pointer bg-white hover:bg-cyan-200 hover:bg-opacity-20 transition" onclick="window.location = '{{ route('projects') . '/' . $project -> id }}'">
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        {{ $project -> title }}
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        {{ $project -> client -> name }}
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        <x-status :item="$project" />
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        {{ $project -> users_count }}
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        {{ $project -> tasks_count }}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="md:ml-22 my-8">
            {{ $projects -> onEachSide(1) -> links() }}
        </div>
    @else
        <p>No projects found.</p>
    @endif
</div>
