<div class="flex flex-col my-4">
    @if ($team)
        <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                <div class="overflow-hidden border border-gray-200 sm:rounded-xl">
                    <table class="min-w-full divide-y divide-gray-100">
                        <thead class="bg-gray-50">
                            <tr>
                                <th scope="col">
                                    Name
                                </th>
                                <th scope="col">
                                    Role
                                </th>
                                <th scope="col">
                                    Projects
                                </th>
                            </tr>
                        </thead>
                        <tbody class="bg-white divide-y divide-gray-100 text-sm">
                            @foreach ($team as $user)
                                <tr class="bg-white hover:bg-cyan-200 hover:bg-opacity-20 transition">
                                    <td class="px-6 py-4 whitespace-nowrap flex flex-row items-center space-x-4">
                                        <x-avatar :src="$user -> getAvatar()" />
                                        <div class="flex flex-col">
                                            <span class="text-sm font-semibold text-gray-900">{{ $user -> name }}</span>
                                            <span class="text-sm text-gray-500">{{ $user -> email}}</span>
                                        </div>
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        {{ $user -> role -> name }}
                                    </td>
                                    <td class="px-6 py-4 whitespace-nowrap">
                                        {{ $user -> projects_count }}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="md:ml-22 my-8">
            {{ $team -> onEachSide(1) -> links() }}
        </div>
    @endif
</div>
