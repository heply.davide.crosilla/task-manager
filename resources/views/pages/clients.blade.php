<x-app-layout>

    <div
        class="w-full flex items-center justify-between my-4"
        x-data="{ show: false }"
        x-init="
            Livewire.on('clientCreated', () => {
                show = false;
            });
        "
    >
        @if (auth() -> user() -> isProjectManager())
            <a
                @click="show = true"
                class="cursor-pointer ml-auto mr-0 flex flex-row items-center px-4 py-2 rounded-xl font-semibold text-cyan-400 hover:text-cyan-500 hover:bg-cyan-200 hover:bg-opacity-20 transition"
            >
                <svg class="h-5 w-5 mr-1" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm1-11a1 1 0 10-2 0v2H7a1 1 0 100 2h2v2a1 1 0 102 0v-2h2a1 1 0 100-2h-2V7z" clip-rule="evenodd" />
                </svg>
                Add a client
            </a>

            <div x-cloak>
                <livewire:create-client />
            </div>
        @endif

    </div>

    <livewire:clients-table />

</x-app-layout>
