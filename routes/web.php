<?php

use App\Http\Controllers\ProjectController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return redirect() -> route('projects'); });

Route::get('/projects', [ProjectController::class, 'index']) -> middleware(['auth']) -> name('projects');
Route::get('/projects/{project:id}', [ProjectController::class, 'show']) -> middleware(['auth']) -> name('project');

Route::get('/team', function () { return view('pages.team'); }) -> middleware(['auth']) -> name('team');

Route::get('/clients', function () { return view('pages.clients'); }) -> middleware(['auth']) -> name('clients');

require __DIR__.'/auth.php';
